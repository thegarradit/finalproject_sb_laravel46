<?php

namespace App\Http\Controllers;

use App\Models\ListBook;
use App\Models\Member;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\User;
use \PDF;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaction = Transaction::All();

        return view('transaksi.list', ['transaction' => $transaction]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $buku = ListBook::All();
        $mahasiswa = Member::All();
        $users = User::All();

        return view('transaksi.tambah', ['buku' => $buku, 'mahasiswa' => $mahasiswa, 'users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'buku_id' => 'required',
            'mahasiswa_id' => 'required',
            'tgl_pinjam' => 'required',
            'user_id' => 'required',
        ]);

        $transaction = new Transaction;

        $transaction->buku_id = $request->input('buku_id');
        $transaction->mahasiswa_id = $request->input('mahasiswa_id');
        $transaction->tanggal_pinjam = $request->input('tgl_pinjam');
        $transaction->user_id = $request->input('user_id');



        $transaction->save();

        return redirect('/transaction')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::find($id);
        $buku = ListBook::All();
        $mahasiswa = Member::All();
        $users = User::All();

        return view('transaksi.detail', ['buku' => $buku, 'mahasiswa' => $mahasiswa, 'users' => $users, 'transaction' => $transaction]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::find($id);
        $buku = ListBook::All();
        $mahasiswa = Member::All();
        $users = User::All();

        return view('transaksi.edit', ['transaction' => $transaction, 'buku' => $buku, 'mahasiswa' => $mahasiswa, 'users' => $users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'mahasiswa_id' => 'required',
            'tgl_pinjam' => 'required',
            'user_id' => 'required',
        ]);

        $transaction = Transaction::find($id);

        $transaction->mahasiswa_id = $request->input('mahasiswa_id');
        $transaction->tanggal_pinjam = $request->input('tgl_pinjam');
        $transaction->tanggal_kembali = $request->input('tgl_kembali');
        $transaction->user_id = $request->input('user_id');

        $transaction->save();

        return redirect('/transaction')->with('success', 'Data Berhasil Diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::find($id);

        $transaction->delete();

        return redirect('/transaction')->with('success', 'Data Berhasil Dihapus');
    }
    public function cetak()
    {
        $riwayat_peminjaman = Transaction::with('user', 'listbook', 'member')->get();
        $pdf = PDF::loadView('transaksi.laporan_pdf', ['riwayat_peminjaman' => $riwayat_peminjaman]);

        return $pdf->download('laporan_transaksi.pdf');

        // return view('transaksi.laporan_pdf', ['riwayat_peminjaman' => $riwayat_peminjaman]);
    }
}
