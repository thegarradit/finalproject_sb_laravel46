<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ListBook;
use App\Models\Kategori;
use Illuminate\Support\Facades\File; 

class BukuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show', 'search']);
    }

    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listbook = ListBook::get();

        return view('buku.tampil', ['listbook' => $listbook]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::get();
        return view('buku.tambah', ['kategori' => $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'kode_buku' => 'required',
            'judul' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun_terbit' => 'required',
            'deskripsi' => 'required',
            'kategori_id' => 'required',
            'lokasi_rak' => 'required',
            'gambar' => 'required|image|mimes:png,jpg,jpeg'
        ]);

        $nama_gambar = time() . '.' . $request->gambar->extension();

        $request->gambar->move(public_path('images'), $nama_gambar);

        $listbook = new ListBook;
        $listbook->kode_buku = $request->kode_buku;
        $listbook->judul = $request->judul;
        $listbook->pengarang = $request->pengarang;
        $listbook->penerbit = $request->penerbit;
        $listbook->tahun_terbit = $request->tahun_terbit;
        $listbook->deskripsi = $request->deskripsi;
        $listbook->kategori_id = $request->kategori_id;
        $listbook->lokasi_rak = $request->lokasi_rak;
        $listbook->gambar = $nama_gambar;

        $listbook->save();
        return redirect('/buku')->with('success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listbook = ListBook::find($id);

        return view('buku.detail', ['listbook' => $listbook]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $listbook = ListBook::find($id);
        $kategori = Kategori::get();

        return view('buku.edit', ['listbook' => $listbook, 'kategori' => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'kode_buku' => 'required',
            'judul' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun_terbit' => 'required',
            'deskripsi' => 'required',
            'kategori_id' => 'required',
            'lokasi_rak' => 'required',
            'gambar' => 'image|mimes:png,jpg,jpeg'
        ]);

        $listbook = ListBook::find($id);

        if ($request->has('gambar')) {
            $path = 'images/';
            File::delete($path . $listbook->gambar);
            $nama_gambar = time() . '.' . $request->gambar->extension();

            $request->gambar->move(public_path('images'), $nama_gambar);
            $listbook->gambar = $nama_gambar;
            $listbook->save();
        }
        $listbook->kode_buku = $request['kode_buku'];
        $listbook->judul = $request['judul'];
        $listbook->pengarang = $request['pengarang'];
        $listbook->penerbit = $request['penerbit'];
        $listbook->tahun_terbit = $request['tahun_terbit'];
        $listbook->deskripsi = $request['deskripsi'];
        $listbook->kategori_id = $request['kategori_id'];
        $listbook->lokasi_rak = $request['lokasi_rak'];


        $listbook->save();
        return redirect('/buku')->with('success', 'Data Berhasil Diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $listbook = ListBook::find($id);
        $path = 'images/';
        File::delete($path . $listbook->gambar);
        $listbook->delete();
        return redirect('/buku')->with('success', 'Data Berhasil Dihapus');
    }
}
