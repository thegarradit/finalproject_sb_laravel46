<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    public function create()
    {
        
        return view('mahasiswa.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'jurusan' => 'required',
            'alamat' => 'required',
            'no_telepon' => 'required'
        ]);

        DB::table('mahasiswa')->insert([
            'nama' => $request->input('nama'),
            'jurusan' => $request->input('jurusan'),
            'alamat' => $request->input('alamat'),
            'no_telepon' => $request->input('no_telepon')
        ]);

        return redirect('/mahasiswa')->with('success', 'Data Berhasil Disimpan');
    }

    public function index()
    {
        $mahasiswa = DB::table('mahasiswa')->get();
 
        return view('mahasiswa.tampil', ['mahasiswa' => $mahasiswa]);
    }

    public function show($id)
    {
        $mahasiswa = DB::table('mahasiswa')->find($id);

        return view('mahasiswa.detail', ['mahasiswa' => $mahasiswa]);
    }

    public function edit($id)
    {
        $mahasiswa = DB::table('mahasiswa')->find($id);

        return view('mahasiswa.edit', ['mahasiswa' => $mahasiswa]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'jurusan' => 'required',
            'alamat' => 'required',
            'no_telepon' => 'required'
        ]);

        DB::table('mahasiswa')
              ->where('id', $id)
              ->update(
                [
                'nama' => $request->input('nama'),
                'jurusan' => $request->input('jurusan'),
                'alamat' => $request->input('alamat'),
                'no_telepon' => $request->input('no_telepon'),
                ]
            );
            
        return redirect('/mahasiswa')->with('success', 'Data Berhasil Diedit');
    }

    public function destroy($id)
    {
        DB::table('mahasiswa')->where('id', '=', $id)->delete();

        return redirect('/mahasiswa')->with('success', 'Data Berhasil Dihapus');
    }
}
