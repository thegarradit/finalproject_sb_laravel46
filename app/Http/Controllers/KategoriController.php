<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kategori;

class KategoriController extends Controller
{
    public function create()
    {
        return view('kategori.tambah');
    }
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required',
        ]);

        DB::table('kategori')->insert([
            'nama' => $request['nama']
        ]);
        return redirect('/kategori')->with('success', 'Data Berhasil Disimpan');
    }
    public function index()
    {   
        $kategori = DB::table('kategori')->get();

        return view('kategori.tampil', ['kategori' => $kategori]);
    }

    public function show($id) 
    {
        $kategori = Kategori::find($id);

        return view('kategori.detail', ['kategori' => $kategori]);
    }

    public function edit($id)
    {
        $kategori = DB::table('kategori')->where('id', $id)->first();
        return view('kategori.edit', ['kategori' => $kategori]);
    }
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nama' => 'required'
        ]);

        DB::table('kategori')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
            ]);
        return redirect('/kategori')->with('success', 'Data Berhasil Diedit');
    }
    public function destroy($id)
    {
        DB::table('kategori')->where('id', $id)->delete();
        return redirect('/kategori')->with('success', 'Data Berhasil Dihapus');
    }
}
