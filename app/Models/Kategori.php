<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $fillable = ['nama'];
    
    public function listbook()
        {
            return $this->hasMany(ListBook::class, 'kategori_id');
        }
}
