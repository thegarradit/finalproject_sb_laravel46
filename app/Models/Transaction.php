<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $table = 'transaksi';

    protected $fillable = ['buku_id', 'mahasiswa_id', 'tanggal_pinjam', 'tanggal_kembali', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'mahasiswa_id');
    }

    public function listbook()
    {
        return $this->belongsTo(ListBook::class, 'buku_id');
    }
}
