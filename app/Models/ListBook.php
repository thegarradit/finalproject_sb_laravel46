<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListBook extends Model
{
    use HasFactory;
    protected $table = 'buku';

    protected $fillable = ['kode_buku', 'judul', 'pengarang', 'penerbit', 'tahun_terbit', 'deskripsi', 'kategori_id', 'lokasi_rak', 'gambar'];

    public function kategori() {
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }
}
