<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;
    protected $table = 'mahasiswa';

    protected $fillable = ['nama', 'jurusan', 'alamat', 'no_telepon'];

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }
}
