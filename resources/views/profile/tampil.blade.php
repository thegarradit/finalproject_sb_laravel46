@extends('layout.master')
  
@section('title')
    Profil Admin
@endsection

@section('content')
<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            <h2>Data Profile</h2>
            <div class="row">
            <div class="col-6">
            <img class="card-img-top my-4" src="{{asset('images/' . $profile->foto)}}" alt="Card image cap">
            </div>
            </div>
            <table class="table">
                <tbody>
                  <tr>
                    <th scope="row">Nama</th>
                    <td>{{$profile->nama}}</td>
                  </tr>
                  <tr>
                    <th scope="row">Alamat</th>
                    <td>{{$profile->alamat}}</td>
                  </tr>
                  <tr>
                    <th scope="row">No. Telepon</th>
                    <td>{{$profile->no_telepon}}</td>
                  </tr>
                  <tr>
                    <th scope="row">Email</th>
                    <td>{{$profile->user->email}}</td>
                  </tr>
                </tbody>
              </table>
              <a href="/" class="btn btn-secondary btn-sm">Kembali</a>
            {{-- <div><h3>Nama : {{$profile->nama}}</h3></div>
            <div><h3>Alamat : {{$profile->alamat}}</h3></div>
            <div><h3>Nomor Telepon : {{$profile->no_telepon}}</h3></div>
            <div><h3>Email : {{$profile->user->email}}</h3></div> --}}
            {{-- <form action="/profile/{{$profile->id}}" method="POST">
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form> --}}
        </div>
    </div>
</div>
@endsection