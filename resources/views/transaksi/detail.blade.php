@extends('layout.master')
  
@section('title')
    Detail Transaksi
@endsection

@section('content')

<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            <table class="table">
                <thead>
                  <tr>
                    <h2 class="mb-4">Detail</h2>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Buku</th>
                    <td>#{{$transaction->listbook->kode_buku}} "{{$transaction->listbook->judul}}"</td>
                  </tr>
                  <tr>
                    <th scope="row">Peminjam</th>
                    <td>{{$transaction->member->nama}}</td>
                  </tr>
                  <tr>
                    <th scope="row">Admin</th>
                    <td>{{$transaction->user->name}}</td>
                  </tr>
                </tbody>
              </table>
            <div class="d-flex justify-content-between">
                <a href="/transaction/{{$transaction->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <a href="/transaction" class="btn btn-secondary btn-sm">Kembali</a>
                
            </div>
              
        </div>
    </div>
</div>

@endsection