@extends('layout.master')
  
@section('title')
    Data Transaksi
@endsection

@push('styles')
    <link href="https://cdn.datatables.net/v/bs4/dt-1.13.4/datatables.min.css" rel="stylesheet"/>
@endpush

@push('scripts')
    <script src="https://cdn.datatables.net/v/bs4/dt-1.13.4/datatables.min.js"></script>
    <script>
        $('#myTable2').DataTable();
    </script>
@endpush

@section('content')
<div class="container">
  <div class="card contentform">
    <div class="card-body my-4">
      <h2>Data Transaksi</h2>
        <table id="myTable2" class="table table-bordered table-striped my-4">
        {{-- <table id="myTable2" class="table table-hover"> --}}
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Detail</th>
              <th scope="col">Tanggal Peminjaman</th>
              <th scope="col">Tanggal Pengembalian</th>
              <th scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($transaction as $key => $value)
            <tr>
              <th scope="row">{{$key+1}}</th>
              <td><a href="/transaction/{{$value->id}}" class="btn btn-secondary btn-sm">Detail</a></td>
              {{-- <td><button data-id="{{$value->id}}" class="btn btn-secondary btn-sm btn-detail">Detail</button></td> --}}
              <td>{{$value->tanggal_pinjam}}</td>
              <td>{{$value->tanggal_kembali}}</td>
              <td>
                <form action="/transaction/{{$value->id}}" method="POST">
                  @csrf
                  @method('delete')
                  <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
              </td>
            </tr>      
            @empty
            
            @endforelse
          </tbody>
      </table>
      <div class="d-flex justify-content-end">
        <a href="/transaction/create" class="btn btn-sm bg-warning"><i class="fa fa-plus" aria-hidden="true"></i></a>
        <a href="/cetak_transaction" target="_blank" class="btn btn-sm bg-primary ml-1"><i class="fa fa-print" aria-hidden="true"></i></a>
      </div>
    </div>
  </div>
</div>
@include('sweetalert::alert')
@endsection