@extends('layout.master')
  
@section('title')
    Tambah Transaksi
@endsection

@section('content')
<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            <form action="/transaction" method="POST" class="form">
                @csrf
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Buku Dipinjam</label>
                    <select class="custom-select" name="buku_id">
                        <option value="">--Pilih--</option>
                        @forelse ($buku as $item)
                        <option value="{{$item->id}}">{{$item->judul}}</option>
                        @empty
                        <option value="nope">Tidak ada buku</option>
                        @endforelse
                    </select>
                </div>
                @error('buku_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Mahasiswa Peminjam</label>
                    <select class="custom-select" name="mahasiswa_id">
                        <option value="">--Pilih--</option>
                        @forelse ($mahasiswa as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        @empty
                        <option value="nope">Tidak ada member</option>
                        @endforelse
                    </select>
                </div>
                @error('mahasiswa_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                    <label for="exampleFormControlInput1">Tanggal Peminjaman</label>
                    <input type="date" class="form-control @error('tgl_pinjam') is-invalid @enderror" name="tgl_pinjam">
                </div>
                @error('tgl_pinjam')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Penanggung Jawab</label>
                    <select class="custom-select" name="user_id">
                        <option value="">--Pilih--</option>
                        @forelse ($users as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @empty
                        <option value="nope">Tidak ada admin</option>
                        @endforelse
                    </select>
                </div>
                @error('user_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-info">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection