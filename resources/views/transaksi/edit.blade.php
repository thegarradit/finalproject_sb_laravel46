@extends('layout.master')
  
@section('title')
    Edit Transaksi
@endsection

@section('content')
<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            <form action="/transaction/{{$transaction->id}}" method="POST" class="form">
                @csrf
                @method('put')
                @error('buku_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Mahasiswa Peminjam</label>
                    <select class="custom-select" name="mahasiswa_id">
                        <option value="">--Pilih--</option>
                        @forelse ($mahasiswa as $item)
                            @if ($item->id === $transaction->mahasiswa_id)
                            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                            @else
                            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                            @endif
                        @empty
                        <option value="nope">Tidak ada member</option>
                        @endforelse
                    </select>
                </div>
                @error('mahasiswa_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                    <label for="exampleFormControlInput1">Tanggal Peminjaman</label>
                    <input type="date" class="form-control @error('tgl_pinjam') is-invalid @enderror" name="tgl_pinjam" value="{{$transaction->tanggal_pinjam}}">
                </div>
                @error('tgl_pinjam')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                    <label for="exampleFormControlInput1">Tanggal Pengembalian</label>
                    <input type="date" class="form-control" name="tgl_kembali" value="{{$transaction->tanggal_kembali}}">
                </div>

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Penanggung Jawab</label>
                    <select class="custom-select" name="user_id">
                        <option value="">--Pilih--</option>
                        @forelse ($users as $item)
                            @if ($item->id === $transaction->user_id)
                                <option value="{{$item->id}}" selected>{{$item->name}}</option>
                            @else
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endif
                        @empty
                        <option value="nope">Tidak ada admin</option>
                        @endforelse
                    </select>
                </div>
                @error('user_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-info">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection