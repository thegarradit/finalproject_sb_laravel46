  <!--header-->
  <header class="main-header clearfix" role="header">
    <div class="logo">
      <a href="/"><em>Sanber</em>Library</a>
    </div>
    <a href="#menu" class="menu-link"><i class="fa fa-bars"></i></a>
    <nav id="menu" class="main-nav" role="navigation">
      <ul class="main-menu">
        <li><a href="/">Home</a></li>
        <li class="has-submenu"><a href="#">List Data</a>
          <ul class="sub-menu">
            <li><a href="/buku">Data Buku</a></li>
            @auth
              <li><a href="/transaction">Data Transaksi</a></li>
              <li><a href="/mahasiswa">Data Mahasiswa</a></li>
            @endauth
            <li><a href="/kategori">Data Kategori</a></li>
          </ul>
        </li>
        @auth
          <li class="has-submenu"><a href="#">Profile</a>
            <ul class="sub-menu">
              <li><a href="/profile/{{ Auth::user()->id }}">{{ Auth::user()->name }}</a></li>
            </ul>
          </li>
        @endauth
        

        {{-- Logout --}}
        @auth
          <li>
          <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
            Logout
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
          </form>
        </li>
        @endauth
        

        <!-- <li><a href="#section5">Video</a></li> -->
        {{-- <li><a href="#section6">Contact</a></li> --}}
        {{-- <li><a href="https://templatemo.com" class="external">External</a></li> --}}
      </ul>
    </nav>
  </header>