@extends('layout.master')
  
@section('title')
    Mengedit Data Mahasiswa
@endsection

@section('content')

<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            <form action="/mahasiswa/{{$mahasiswa->id}}" method="POST" class="form">
                @csrf
                @method('PUT')
                <div class="form-group">
                <h2 class="mb-4">Formulir Data Mahasiswa</h2>
                <label>Nama Lengkap</label>
                <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{$mahasiswa->nama}}">
                </div>
                @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                <label>Jurusan</label>
                <input type="text" name="jurusan" class="form-control @error('jurusan') is-invalid @enderror" value="{{$mahasiswa->jurusan}}">
                </div>
                @error('jurusan')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                <label>Alamat</label>
                <textarea name="alamat" class="form-control @error('alamat') is-invalid @enderror" cols="30" rows="10">{{$mahasiswa->alamat}}</textarea>
                </div>
                @error('alamat')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="form-group">
                <label>No Telepon</label>
                <input type="text" name="no_telepon" class="form-control @error('no_telepon') is-invalid @enderror" value="{{$mahasiswa->no_telepon}}">
                </div>
                @error('no_telepon')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection