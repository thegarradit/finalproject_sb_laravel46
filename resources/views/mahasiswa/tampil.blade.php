@extends('layout.master')
  
@section('title')
    Data Mahasiswa
@endsection

@push('styles')
    <link href="https://cdn.datatables.net/v/bs4/dt-1.13.4/datatables.min.css" rel="stylesheet"/>
@endpush

@push('scripts')
    <script src="https://cdn.datatables.net/v/bs4/dt-1.13.4/datatables.min.js"></script>
    <script>
        $('#myTable').DataTable();
    </script>
@endpush

@section('content')

<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            <a href="/mahasiswa/create" class="btn btn-primary btn-sm mb-4">Tambah Mahasiwa</a>
            <h2>Data Mahasiswa</h2>
            <table id="myTable" class="table table-bordered table-striped my-4">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                    @forelse ($mahasiswa as $key => $value)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$value->nama}}</td>
                            <td>
                                
                                <form action="/mahasiswa/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <a href="/mahasiswa/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                                    <a href="/mahasiswa/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('sweetalert::alert')

@endsection