@extends('layout.master')
  
@section('title')
    Detail Mahasiswa
@endsection

@section('content')

<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            <table class="table">
                <thead>
                  <tr>
                    <h2 class="mb-4">Detail Mahasiswa</h2>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">Nama</th>
                    <td>{{$mahasiswa->nama}}</td>
                  </tr>
                  <tr>
                    <th scope="row">Jurusan</th>
                    <td>{{$mahasiswa->jurusan}}</td>
                  </tr>
                  <tr>
                    <th scope="row">Alamat</th>
                    <td>{{$mahasiswa->alamat}}</td>
                  </tr>
                  <tr>
                    <th scope="row">No. Telepon</th>
                    <td>{{$mahasiswa->no_telepon}}</td>
                  </tr>
                </tbody>
              </table>
              <a href="/mahasiswa" class="btn btn-secondary btn-sm">Kembali</a>
        </div>
    </div>
</div>

@endsection