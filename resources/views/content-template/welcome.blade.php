@extends('layout.master')
  
@section('title')
  Welcome
@endsection

@section('content')
<!-- ***** Main Banner Area Start ***** -->
<section class="section main-banner" id="top" data-section="section1">
  <video autoplay muted loop id="bg-video">
    <source src="{{asset('/template/assets/images/course-video.mp4')}}" type="video/mp4" />
  </video>

  <div class="video-overlay header-text">
    <div class="caption">
      <h6>Places where people like to borrow books</h6>
      <h2><em>Sanber</em> Library</h2>
      <div class="main-button">
        @guest
          <div><a>Join as Member</a></div>
        @endguest
        @auth
        <div><a>Welcome</a></div>
        @endauth
      </div>
    </div>
  </div>
</section>
<!-- ***** Main Banner Area End ***** -->

<section class="features">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-12">
        <div class="features-post">
          <div class="features-content">
            <div class="content-show">
              <h4><i class="fa fa-question"></i>About Us</h4>
            </div>
            <div class="content-hide">
              <p class="text-justify">Sanber Library merupakan nama website dari perpustakaan sanber. Yang dimana bertujuan untuk meningkatkan minat baca mahasiswa dengan peminjaman buku secara gratis.</p>
              <p class="hidden-sm text-justify">Sanber Library merupakan nama website dari perpustakaan sanber. Yang dimana bertujuan untuk meningkatkan minat baca mahasiswa dengan peminjaman buku secara gratis.</p>
          </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-12">
        <div class="features-post second-features">
          <div class="features-content">
            <div class="content-show">
              <h4><i class="fa fa-book"></i>Our Book</h4>
            </div>
            <div class="content-hide">
              <p class="text-justify">Sanber library menyediakan berbagai macam buku, mulai dari buku pengetahuan, novel, kamus, biografi, sampai ke ensiklopedia dan dapat dipinjam dalam waktu yang cukup lama.</p>
              <p class="hidden-sm text-justify">Sanber library menyediakan berbagai macam buku, mulai dari buku pengetahuan, novel, kamus, biografi, sampai ke ensiklopedia dan dapat dipinjam dalam waktu yang cukup lama.</p>
          </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-12">
        <div class="features-post third-features">
          <div class="features-content">
            <div class="content-show">
              <h4><i class="fa fa-envelope"></i>Contact Us</h4>
            </div>
            <div class="content-hide">
              <p class="text-justify">info lebih lanjut dapat menghubungi:<br>
                Admin 1: <a href="https://t.me/TegarRH">Tegar</a><br>
                Admin 2: <a href="https://t.me/unknowncreature404">Fitrah</a><br>
                Admin 3: <a href="https://t.me/noeralif">Alif</a>
              </p>
              <p class="hidden-sm text-justify">info lebih lanjut dapat menghubungi:<br>
                Admin 1: <a href="https://t.me/TegarRH">Tegar</a><br>
                Admin 2: <a href="https://t.me/unknowncreature404">Fitrah</a><br>
                Admin 3: <a href="https://t.me/noeralif">Alif</a>
              </p>
              <p>
                <a href="/login">Login as an Admin</a>
              </p>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section why-us" data-section="section2">
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="section-heading">
                  <h2>Manfaat Membaca Buku</h2>
              </div>
          </div>
      <div class="col-md-12">
          <div id='tabs'>
              <ul>
                  <li><a href='#tabs-1'>Menambah Wawasan & Pengetahuan</a></li>
                  <li><a href='#tabs-2'>Mengurangi Stres</a></li>
                  <li><a href='#tabs-3'>Meningkatkan Kualitas Memori</a></li>
              </ul>
              <section class='tabs-content'>
                  <article id='tabs-1'>
                      <div class="row">
                          <div class="col-md-6">
                              <img src="{{asset('/template/assets/images/choose-us-image-01.png')}}" alt="">
                          </div>
                          <div class="col-md-6">
                              <h4>Menambah Wawasan & Pengetahuan</h4>
                              <p class="text-justify">“ Buku adalah Jendela Dunia”. Pengertian kata mutiara tersebut adalah di dalam buku tersedia berbagai wawasan dan pengetahuan. Untuk mendapatkannya, kita harus membaca buku. Sehingga jika kita membaca buku,  wawasan dan pengetahuan kita bertambah.</p>
                          </div>
                      </div>
                  </article>
                  <article id='tabs-2'>
                      <div class="row">
                          <div class="col-md-6">
                              <img src="{{asset('/template/assets/images/choose-us-image-02.png')}}" alt="">
                          </div>
                          <div class="col-md-6">
                              <h4>Mengurangi Stres</h4>
                              <p class="text-justify">Selain mampu menstimulasi kesehatan otak dan mental, kegunaan buku juga dapat mengurangi risiko stres. Hal ini karena wajar bila manusia merasa lelah ketika berkegiatan sehari-hari. Tak jarang mereka merasa terlalu lelah hingga stres. Dengan membiasakan diri untuk membaca buku, maka dapat menekan perkembangan hormon stres.</p>
                          </div>
                      </div>
                  </article>
                  <article id='tabs-3'>
                      <div class="row">
                          <div class="col-md-6">
                              <img src="{{asset('/template/assets/images/choose-us-image-03.png')}}" alt="">
                          </div>
                      <div class="col-md-6">
                          <h4>Meningkatkan Kualitas Memori</h4>
                          <p class="text-justify">Kebiasaan membaca buku akan memengaruhi setiap memori dan membantu otak untuk ditempa semakin kuat. Tak heran jika kegiatan membaca buku dapat membuat seseorang lebih mudah mengingat suatu hal daripada yang tidak pernah membaca buku.</p>
                      </div>
                      </div>
                  </article>
              </section>
          </div>
      </div>
      </div>
  </div>
</section>
@endsection
