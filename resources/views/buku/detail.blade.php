@extends('layout.master')

@section('title')
Detail Buku
@endsection

@section('content')

<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            <img class="card-img-top" src="{{asset('images/' . $listbook->gambar)}}" alt="Card image cap">
            <h5 class="card-title">{{$listbook->judul}}</h5>
            <h5 class="card-title">Kategori</h5>
            <p class="card-text">{{$listbook->kategori->nama}}</p>
            <h5 class="card-title">Deskripsi</h5>
            <p class="card-text">{{$listbook->deskripsi}}</p>
            <h5 class="card-title">Pengarang</h5>
            <p class="card-text">{{$listbook->pengarang}}</p>
            <h5 class="card-title">Penerbit</h5>
            <p class="card-text">{{$listbook->penerbit}}</p>
            <h5 class="card-title">Tahun Terbit</h5>
            <p class="card-text">{{$listbook->tahun_terbit}}</p>
            <h5 class="card-title">Lokasi Rak</h5>
            <p class="card-text">{{$listbook->lokasi_rak}}</p>
            <h5 class="card-title">Status</h5>
            <p class="card-text">{{$listbook->status}}</p>
            <a href="/buku" class="btn btn-secondary btn-sm">Kembali</a>
        </div>
    </div>
</div>
@endsection