@extends('layout.master')

@section('title')
Edit Data Buku
@endsection

@section('content')

<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            <form action="/buku/{{$listbook->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                  <label>Kode Buku</label>
                  <input type="text" name="kode_buku" class="form-control @error('kode_buku') is-invalid @enderror" value="{{$listbook->kode_buku}}">
                </div>
                @error('kode_buku')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Judul</label>
                    <input type="text" name="judul" class="form-control @error('judul') is-invalid @enderror" value="{{$listbook->judul}}">
                  </div>
                  @error('judul')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label>Pengarang</label>
                    <input type="text" name="pengarang" class="form-control @error('pengarang') is-invalid @enderror" value="{{$listbook->pengarang}}">
                  </div>
                  @error('pengarang')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label>Penerbit</label>
                    <input type="text" name="penerbit" class="form-control @error('penerbit') is-invalid @enderror" value="{{$listbook->penerbit}}">
                  </div>
                  @error('penerbit')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label>Tahun Terbit</label>
                    <input type="number" name="tahun_terbit" class="form-control @error('tahun_terbit') is-invalid @enderror" value="{{$listbook->tahun_terbit}}">
                  </div>
                  @error('tahun_terbit')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                <div class="form-group">
                    <label >Deskripsi</label>
                    <textarea name="deskripsi" id="" cols="30" rows="10" class="form-control @error('deskripsi') is-invalid @enderror ">{{$listbook->deskripsi}}</textarea>
                </div>
                @error('deskripsi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Kategori</label>
                    <select name="kategori_id" id="" class="form-control">
                        <option value="">Pilih Kategori</option>
                        @forelse ($kategori as $item)
                            @if ($item->id === $listbook->kategori_id)
                                <option value="{{$item->id}}" selected>{{$item -> nama}}</option>
                            @else
                                <option value="{{$item->id}}">{{$item -> nama}}</option>
                            @endif
                        @empty
                            <option value="">Tidak Ada Data</option>
                        @endforelse
                    </select>
                  </div>
                  @error('kategori_id')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label>Lokasi Rak</label>
                    <input type="text" name="lokasi_rak" class="form-control @error('lokasi_rak') is-invalid @enderror" value="{{$listbook->lokasi_rak}}">
                  </div>
                  @error('lokasi_rak')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label>Gambar</label>
                    <input type="file" name="gambar" class="form-control @error('gambar') is-invalid @enderror">
                  </div>
                  @error('gambar')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        </div>
    </div>
</div>

@endsection