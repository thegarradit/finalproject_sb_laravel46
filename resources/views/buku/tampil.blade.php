@extends('layout.master')

@section('title')
List Buku
@endsection

@section('content')

<div class="container">
    <div class="card contentform">
        <div class="card-body my-2">
            @auth
                 <a href="/buku/create" class="btn btn-primary btn-sm mb-4">Tambah Buku</a>
            @endauth
            <h2>List Buku</h2>
                <div>
                   <h4 class="mt-4">Cari Berdasarkan Kategori <a href="/kategori" id="search-link">Disini.</a></h4> 
                </div>
            </div>
            <div class="row mx-1">
                @forelse ($listbook as $item)
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="card mt-2 mb-5">
                        <img class="card-img-top" src="{{asset('images/' . $item->gambar)}}" alt="Card image cap" height="500px">
                        <div class="card-body">
                        <h5 class="card-title">{{$item->judul}}</h5>
                        <p class="card-text">{{Str::limit ($item->deskripsi, 50)}}</p>
                        <a href="/buku/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Detail Buku</a>
                        @auth
                          <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <a href="/buku/{{$item->id}}/edit" class="btn btn-info btn-block btn-sm">Edit</a>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <form action="/buku/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit"  value="Delete" class="btn btn-danger btn-block btn-sm">
                                </form>
                            </div>
                        </div>  
                        @endauth
                        </div>
                    </div>
                </div>
                @empty
                    <h5 class="ml-3 mt-3">Tidak ada data yang dapat ditampilkan</h5>
                @endforelse
            </div>
        </div>
    </div>
</div>
@include('sweetalert::alert')

@endsection