@extends('layout.master')

@section('title')
Data Kategori
@endsection

@section('content')

<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            <form action="/kategori" method="POST" class="form">
                @csrf
                <div class="form-group">
                    <h2 class="mb-4"> Data Kategori</h2>
                    <label>Nama Kategori</label>
                    <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror">
                </div>
                @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection