@extends('layout.master')

@section('title')
    List Buku
@endsection

@section('content')

<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            <h2>List Data Buku</h2>
            <h4>{{$kategori->nama}}</h4>
            <div class="row">
                @forelse ($kategori->listbook as $item)
                <div class="col-lg-4 col-md-6 col-sm-12"> 
                    <div class="card my-2">
                        <img class="card-img-top" src="{{asset('images/' . $item->gambar)}}" alt="Card image cap" height="500px">
                        <div class="card-body">
                        <h5 class="card-title">{{$item->judul}}</h5>
                        <p class="card-text">{{Str::limit ($item->deskripsi, 50)}}</p>
                        <a href="/buku/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Detail Buku</a>
                        @auth
                          <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <a href="/buku/{{$item->id}}/edit" class="btn btn-info btn-block btn-sm">Edit</a>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <form action="/buku/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit"  value="Delete" class="btn btn-danger btn-block btn-sm">
                                </form>
                            </div>
                        </div>  
                        @endauth
                        </div>
                    </div>
                </div>
                @empty
                    <h5 class="ml-3 mt-3">Tidak ada buku yang sesuai dengan kategori {{$kategori->nama}}</h5>
                @endforelse
            </div>
        </div>
    </div>
</div>
@include('sweetalert::alert')

@endsection