@extends('layout.master')

@section('title')
Data Kategori
@endsection

@section('content')

<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            @auth
            <a href="/kategori/create" class="btn btn-primary btn-sm mb-4">Tambah Kategori</a>
            @endauth
            <h2>Data Kategori</h2>
            <table class="table table-bordered table-striped table-dark my-4">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        @auth
                        <th scope="col">Action</th>
                        @endauth
                    </tr>
                </thead>
                <tbody>
                    @forelse ($kategori as $key => $value)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td><a href="/kategori/{{$value->id}}">{{$value->nama}}</a></td>
                    
                        @auth    
                        <td>
                            <form action="/kategori/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/kategori/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>
                        @endauth

                    </tr>
                    @empty
                    <tr>
                        <td>Tidak ada data yang dapat ditampilkan</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('sweetalert::alert')

@endsection