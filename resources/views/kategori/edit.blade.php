@extends('layout.master')

@section('title')
Mengedit Data Mahasiswa
@endsection

@section('content')

<div class="container">
    <div class="card contentform">
        <div class="card-body my-4">
            <form action="/kategori/{{$kategori->id}}" method="POST" class="form">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <h2 class="mb-4">Data Kategori</h2>
                    <label>Nama Lengkap</label>
                    <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{$kategori->nama}}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection