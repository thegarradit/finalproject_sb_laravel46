<?php

use App\Http\Controllers\AssistController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\ProfileController;
use App\Models\Kategori;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Rute Template
Route::get('/', function () {
    return view('content-template.welcome');
});

Route::get('/page2', function () {
    return view('content-template.page2');
});

Route::get('/page3', function () {
    return view('content-template.page3');
});

Route::get('/page4', function () {
    return view('content-template.page4');
});

Route::get('/page5', function () {
    return view('content-template.page5');
});

Route::get('/page6', function () {
    return view('content-template.page6');
});

// Auth
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


// CRUD Transaksi
Route::group(['middleware' => ['auth']], function () {
    Route::resource('transaction', TransactionController::class);
    Route::get('/cetak_transaction', [TransactionController::class, 'cetak']);
});


//CRUD Mahasiswa
Route::group(['middleware' => ['auth']], function () {
    //Create
    //Route mengarah ke form mahasiswa
    Route::get('/mahasiswa/create', [MahasiswaController::class, 'create']);
    //Route untuk menyimpan data
    Route::post('/mahasiswa', [MahasiswaController::class, 'store']);

    //Read
    //Route untuk menampilkan data mahasiswa
    Route::get('/mahasiswa', [MahasiswaController::class, 'index']);
    //Route untuk detail data mahasiswa
    Route::get('/mahasiswa/{id}', [MahasiswaController::class, 'show']);

    //Update
    //Route untuk mengarah ke form data mahasiswa berdasarkan id
    Route::get('/mahasiswa/{id}/edit', [MahasiswaController::class, 'edit']);
    //Route untuk mengupdate data mahasiswa berdasarkan id
    Route::put('/mahasiswa/{id}', [MahasiswaController::class, 'update']);

    //Delete
    //Route untuk menghapus data mahasiswa berdasarkan id
    Route::delete('/mahasiswa/{id}', [MahasiswaController::class, 'destroy']);
});



//CRUD Kategori
Route::group(['middleware' => ['auth']], function () {
    //Create
    Route::get('/kategori/create', [KategoriController::class, 'create']);
    //kirim data ke daatabase
    Route::post('/kategori', [KategoriController::class, 'store']);
    //Update
    Route::get('/kategori/{kategori_id}/edit', [KategoriController::class, 'edit']);
    //kirim simpan data ke database
    Route::put('/kategori/{kategori_id}', [KategoriController::class, 'update']);
    //Delete
    Route::delete('/kategori/{kategori_id}', [KategoriController::class, 'destroy']);
});
//Read
Route::get('/kategori', [KategoriController::class, 'index']);
// Detail Buku Terkait
Route::get('/kategori/{id}', [KategoriController::class, 'show']);


//CRUD Buku
Route::resource('buku', BukuController::class);
Route::get('/buku/search/{id}', [BukuController::class, 'search']);


// CRUD Profile
Route::group(['middleware' => ['auth']], function () {
    Route::resource('profile', ProfileController::class);
    // Route::get('/profile', [AssistController::class, 'index']);
});
